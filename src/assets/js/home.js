// console.log('home page');

export default {
	data() {
		return {
			page: 'home'
		};
	},
	mounted() {
		var context = this;
		//
		var tl = new TimelineMax();
		tl.to($('.page-home'), 0.4, { onComplete: function() {
            arounds();
        }});

        // arounds 
        function arounds() {
        	var tl = new TimelineMax({ });
            tl.to($('.page-home .ingot'), 0.8, { 'right': '-8.4%', ease: Power4.easeOut}, 0);
        	tl.to($('.page-home .lanterns'), 0.8, { 'left': '-14.8%', ease: Power4.easeOut}, 0);
        	tl.to($('.page-home .cloud1'), 0.8, { 'left': '-16.4%', ease: Power4.easeOut}, 0.05);
        	tl.to($('.page-home .cloud2'), 0.8, { 'left': '-16%', ease: Power4.easeOut}, 0.05);
        	tl.to($('.page-home .cloud3'), 0.8, { 'right': '-15.2%', ease: Power4.easeOut}, 0.05);
        	tl.to($('.page-home .cloud4'), 0.8, { 'right': '-10.2%', ease: Power4.easeOut}, 0.05);
        	tl.to($('.page-home .clouds1'), 0.8, { 'bottom': '0.5rem', ease: Power4.easeOut}, 0.1);
        	tl.to($('.page-home .clouds2'), 0.8, { 'bottom': '0', ease: Power4.easeOut}, 0.2);
        	tl.to($('.page-home .text'), 0.8, { 'top': '10%', ease: Power4.easeOut}, 0.1);
        	tl.to($('.page-home .box-mousefulu'), 0.8, { 'right': '-10.6%', ease: Power1.easeOut, onComplete: function() {
        		$('.page-home .arrow').fadeIn(function() {
                    arrowTwinkle();
                });
        		mouseFloat();
        		lanternShake();
        	}}, 0.2);
        };

        // mouse float
        function mouseFloat() {
        	var tl = new TimelineMax({ repeat: -1, yoyo:true });
            tl.to($('.page-home .box-mousefulu'), 1.4, { 'top': '+=0.6rem'});
        };

		// lantern shake
        function lanternShake() {
            var tl = new TimelineMax({ repeat: -1, yoyo: true });
            tl.to($('.page-home .box-mousefulu .lantern'), 1.2, { rotation: 25, ease: Power1.easeOut});
        };

        // arrow twinkle
        function arrowTwinkle() {
            var tl = new TimelineMax({ repeat: -1, yoyo: true });
            tl.to($('.page-home .arrow'), 0.6, { autoAlpha: 1});
        };

        var startx, starty,
	        direction = 'up';

        function getAngle(angx, angy) {
            return Math.atan2(angy, angx) * 180 / Math.PI;
        };

        //根据起点终点返回方向 1向上 2向下 3向左 4向右 0未滑动
        function getDirection(startx, starty, endx, endy) {
            var angx = endx - startx;
            var angy = endy - starty;
            var result = 0;
            //如果滑动距离太短
            if (Math.abs(angx) < 2 && Math.abs(angy) < 2) {
                return result;
            }
            var angle = getAngle(angx, angy);
            if (angle >= -135 && angle <= -45) {
                result = 1;
            } else if (angle > 45 && angle < 135) {
                result = 2;
            } else if ((angle >= 135 && angle <= 180) || (angle >= -180 && angle < -135)) {
                result = 3;
            } else if (angle >= -45 && angle <= 45) {
                result = 4;
            }
            return result;
        };

        // touch start
        function start(e) {
            startx = e.touches[0].pageX;
            starty = e.touches[0].pageY;
        };
        //手指接触屏幕
        document.addEventListener('touchstart', start, false);
        // touch end
        function end(e) {
            var endx, endy;
            endx = e.changedTouches[0].pageX;
            endy = e.changedTouches[0].pageY;
            var direction = getDirection(startx, starty, endx, endy);
            if(direction == 1) {
                document.removeEventListener('touchstart', start, false);
                document.removeEventListener('touchend', end, false);
                context.$router.replace('/level1');
            };
        };
        //手指离开屏幕
        document.addEventListener('touchend', end, false);
	},
	methods: {
		//
	}
};
