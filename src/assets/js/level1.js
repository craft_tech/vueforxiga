// console.log('level1 page');

export default {
	data() {
		return {
			//
		};
	},
	mounted() {
		var context = this;
		//
		var tl = new TimelineMax();
		tl.to($('.page-level1'), 0.4, { onComplete: function() {
            fuluFall($('.page-level1 .box-fulu1 .fulu1'), $('.page-level1 .box-fulu1 .shadow1'));
        }});

		// fulu1 fall
        function fuluFall(el1, el2) {
            var tl = new TimelineMax({ delay: 0.2 });
            tl.to(el1, 0.6, { 'bottom': '0.12rem', ease: Power4.easeIn}, 0);
            tl.to(el2, 0.6, { scale: 1, ease: Power4.easeIn}, 0);
            tl.to(el1, 0.04, { onComplete: function() {
                el1.attr('src', require('../img/level1/fulu1-2.png'));
            }}, 0.61);
            tl.to(el1, 0.04, { onComplete: function() {
                el1.attr('src', require('../img/level1/fulu1-3.png'));
            }}, 0.65);
            tl.to(el1, 0.2, { 'bottom': '1.32rem', ease: Power4.easeOut}, 0.69);
            tl.to(el2, 0.2, { scale: 0.9, ease: Power4.easeOut}, 0.69);
            tl.to(el1, 0.04, { onComplete: function() {
                el1.attr('src', require('../img/level1/fulu1-2.png'));
            }}, 0.7);
            tl.to(el1, 0.04, { onComplete: function() {
                el1.attr('src', require('../img/level1/fulu1-1.png'));
            }}, 0.74);
            tl.to(el1, 0.1, { 'bottom': '0.12rem', ease: Power4.easeIn}, 0.78);
            tl.to(el2, 0.1, { scale: 1, ease: Power4.easeIn}, 0.78);
            tl.to(el1, 0.04, { onComplete: function() {
                el1.attr('src', require('../img/level1/fulu1-2.png'));
            }}, 0.89);
            tl.to(el1, 0.04, { onComplete: function() {
                el1.attr('src', require('../img/level1/fulu1-3.png'));
            }}, 0.93);
            tl.to(el1, 0.04, { onComplete: function() {
                el1.attr('src', require('../img/level1/fulu1-2.png'));
            }}, 0.97);
            tl.to(el1, 0.04, { onComplete: function() {
                el1.attr('src', require('../img/level1/fulu1-1.png'));
            }}, 1.01);
            tl.to(el1, 0.2, { onComplete: function() {
                bmiTwinkle();
                mouseOut1();
            }});
        };

        // bmi twinkle
        function bmiTwinkle() {
            var tl = new TimelineMax({ repeat: -1, yoyo: true, delay: 0.2 });
            tl.to($('.page-level1 .bmi'), 0.4, { autoAlpha: 1});
        };

        // mouse out
        function mouseOut1() {
            var tl = new TimelineMax({ delay: 0.4 });
            tl.to($('.page-level1 .box-cave .box-mouse1'), 0.6, { 'top': '0rem', ease: Power4.easeOut, onComplete: function() {
                eyes1($('.page-level1 .eyes'));
                $('.page-level1 .help').fadeIn('fast', function() {
                    helpShake1($('.page-level1 .help'));
                });
            }});
        };

        // eyes
        function eyes1(el) {
            var tl = new TimelineMax({ repeat: 1});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/common/close.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/common/open.png'));
            }});
        };

        // help shake
        function helpShake1(el) {
            var tl = new TimelineMax({ delay: 0.2, repeat: -1, repeatDelay: 1});
            tl.to(el, 0.35, {'right': '+=2%', ease: Power0.easeOut, repeat: 1, yoyo: true, onStart: function() {
                if(canHelp == 0) {
                    tl.kill();
                };
            }});
            tl.to(el, 0.35, {'right': '+=2%', ease: Power0.easeOut, repeat: 1, yoyo: true});
        };

        // help
        var canHelp = 1; 
        $('.page-level1 .help').off('click');
        $('.page-level1 .help').on('click', function() {
            if(canHelp == 1) {
                canHelp = 0;
                $('.page-level1 .bmi').fadeOut('fast');
                $('.page-level1 .help').fadeOut('fast');
                mouseIn1();
            };
        });

        // mouse in
        function mouseIn1() {
            var tl = new TimelineMax({ delay: 0.4 });
            tl.to($('.page-level1 .box-mouse1'), 0.6, { 'top': '1.74rem', ease: Power4.easeOut});
            tl.to($('.page-level1 .box-cave'), 0.6, {scale: 0, ease: Power4.easeOut, onComplete: function() {
                mouseCome1();
            }});
        };

        // mouse come
        function mouseCome1() {
            var tl = new TimelineMax({ delay: 0.2 });
            tl.to($('.page-level1 .box-boxing'), 0.6, { 'right': '12.8%', ease: Power4.easeOut, onComplete: function() {
                boxing($('.page-level1 .boxing'));
                smoke($('.page-level1 .box-smoke'));
                sugar1();
            }});
        };
        // boxing
        function boxing(el) {
            var tl = new TimelineMax({ repeat: 21, delay: 0.2, onStart: function() {
                boxingFulu($('.page-level1 .box-fulu1 .fulu1'));
            }, onComplete: function() {
                mouseLeave1();
            } });
            tl.to(el, 0.02, { onComplete: function() {
                el.attr('src', require('../img/level1/boxing2.png'));
            }});
            tl.to(el, 0.02, { onComplete: function() {
                el.attr('src', require('../img/level1/boxing3.png'));
            }});
            tl.to(el, 0.02, { onComplete: function() {
                el.attr('src', require('../img/level1/boxing4.png'));
            }});
            tl.to(el, 0.02, { onComplete: function() {
                el.attr('src', require('../img/level1/boxing5.png'));
            }});
            tl.to(el, 0.02, { onComplete: function() {
                el.attr('src', require('../img/level1/boxing6.png'));
            }});
            tl.to(el, 0.02, { onComplete: function() {
                el.attr('src', require('../img/level1/boxing7.png'));
            }});
            tl.to(el, 0.02, { onComplete: function() {
                el.attr('src', require('../img/level1/boxing1.png'));
            }});
        };

        // boxing fulu
        function boxingFulu(el) {
            var tl = new TimelineMax({ delay: 0.2, repeat: 49, yoyo: true});
            tl.to(el, 0.06, { rotation: -4, ease: Power4.easeIn});
        };

        // smoke
        function smoke(el) {
            var tl = new TimelineMax({ repeat: 0, delay: 0.4 });
            tl.to(el, 0.15, { autoAlpha: 1, onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke2.png'));
            }});
            tl.to(el, 0.15, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke3.png'));
            }});
            tl.to(el, 0.15, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke4.png'));
            }});
            tl.to(el, 0.15, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke5.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke4.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke5.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke4.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke5.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke4.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke5.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke4.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke5.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke4.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke5.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke4.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke5.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke4.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke5.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                $('.page-level1 .smoke').attr('src', require('../img/level1/smoke4.png'));
            }});
        };

        // sugar
        function sugar1() {
            var tl = new TimelineMax({ delay: 0.6 });
            tl.to($('.page-level1 .box-sugar1'), 0.1, {'display': 'block'}, 0);
            tl.to($('.page-level1 .sugar1'), 0.4, {'top': '-0.96rem', 'left': '-180%', ease: Power4.easeOut, onComplete: function() {
                $('.page-level1 .box-sugar1').hide();
            }}, 0.02);
            tl.to($('.page-level1 .line1'), 0.08, {autoAlpha: 1, repeat: 1, yoyo: true}, 0.02);

            tl.to($('.page-level1 .box-sugar2'), 0.1, {'display': 'block'}, 1);
            tl.to($('.page-level1 .sugar2'), 0.4, {'top': '-2.6rem', 'left': '-330%', ease: Power4.easeOut, onComplete: function() {
                $('.page-level1 .box-sugar2').hide();
            }}, 1.02);
            tl.to($('.page-level1 .line2'), 0.08, {autoAlpha: 1, repeat: 1, yoyo: true}, 1.02);

            tl.to($('.page-level1 .box-sugar3'), 0.1, {'display': 'block'}, 2);
            tl.to($('.page-level1 .sugar3'), 0.4, {'top': '-2.4rem', 'right': '-300%', ease: Power4.easeOut, onComplete: function() {
                $('.page-level1 .box-sugar3').hide();
            }}, 2.02);
            tl.to($('.page-level1 .line3'), 0.08, {autoAlpha: 1, repeat: 1, yoyo: true}, 2.02);
        };

        // mouse leave
        function mouseLeave1() {
            var tl = new TimelineMax({ delay: 0.4 });
            tl.to($('.page-level1 .box-boxing'), 0.6, { 'right': '-24.6%', ease: Power4.easeOut, onComplete: function() {
                $('.page-level1 .box-boxing').hide();
            }});
            tl.to($('.page-level1 .box-smoke'), 1.2, { autoAlpha: 0, onComplete: function() {
                $('.page-level1 .text1').fadeOut(400);
                $('.page-level1 .box-fulu1').fadeOut(400);
            }});
            tl.to($('.page-level1 .box-smoke'), 0.2, { onComplete: function() {
                $('.page-level1 .re').fadeIn(400, function() {
                    mouseUp1();
                    fuluReShake1($('.page-level1 .re .box-fulumouse'));
                });
            }}, '+=0.4');
        };

        // fulu shake
        function fuluReShake1(el) {
            var tl = new TimelineMax({ repeat: -1, delay: 0.2 });
            tl.to(el, 0.25, { rotation: 6, ease: Power1.easeOut, repeat: 1, yoyo: true});
            tl.to(el, 0.25, { rotation: -6, ease: Power1.easeOut, repeat: 1, yoyo: true});
        };

        // mouse up
        function mouseUp1() {
            var tl = new TimelineMax({ delay: 0.2 });
             tl.to($('.page-level1 .re .box-mouse2'), 0.8, { 'bottom': '3.32rem', 'left': '-30.2%', ease: Power4.easeOut, onComplete: function() {
                paoShake($('.page-level1 .re .box-pao'));
                paoFire($('.page-level1 .re .fire'));
                $('.page-level1 .re .box-tip').fadeIn('fast', function() {
                   tipTwinkle1();
                });
             }});
        };

        // shake
        function paoShake(el) {
            var tl = new TimelineMax({ repeat: -1, delay: 0.1 });
            tl.to(el, 0.4, {rotation: 30, transformOrigin: '25% top', repeat: 1, yoyo: true});
            tl.to(el, 0.2, {rotation: -20, transformOrigin: '25% top', repeat: 1, yoyo: true});
        };

        // fire
        function paoFire(el) {
            var tl = new TimelineMax({ repeat: -1 });
            tl.to(el, 0.15, {autoAlpha: 1, onComplete: function() {
                el.attr('src', require('../img/level1/fire2.png'));
            }});
            tl.to(el, 0.15, {onComplete: function() {
                el.attr('src', require('../img/level1/fire3.png'));
            }});
            tl.to(el, 0.15, {onComplete: function() {
                el.attr('src', require('../img/level1/fire1.png'));
            }});
        };

        // tip
        function tipTwinkle1() {
            var tl = new TimelineMax({ repeat: -1, yoyo: true });
            tl.to($('.page-level1 .re .next'), 0.4, {autoAlpha: 1});
        };
	},
	methods: {
        prev() {
            this.$router.replace('/home');
        },
        next() {
            this.$router.replace('/level2');
        }
	}
};
