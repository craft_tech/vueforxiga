// console.log('level2 page');

export default {
    data() {
        return {
            //
        };
    },
    mounted() {
        var context = this;
        //
        var tl = new TimelineMax();
        tl.to($('.page-level2'), 0.4, { onComplete: function() {
            $('.page-level2 .text1').fadeIn(function() {
                $('.page-level2 .box-arrow1').fadeIn(function() {
                    arrow1Down($('.page-level2 .box-arrow1'));
                });
            });
        }});

        tl.to($('.page-level2'), 0.2, { onComplete: function() {
            mouseOut2();
        } }, '+=1.8');

        // arrow1 down
        function arrow1Down(el) {
            var tl = new TimelineMax({ delay: 0.2, repeat: -1, repeatDelay: 0.2 });
            tl.to(el, 1.6, { autoAlpha: 0, 'top': '+=1rem', ease: Power4.easeOut});
        };

        // mouse out
        function mouseOut2() {
            var tl = new TimelineMax({ });
            tl.to($('.page-level2 .box-mouse1'), 0.6, { 'top': '0rem', ease: Power4.easeOut, onComplete: function() {
                eyes2($('.page-level2 .eyes'));
                $('.page-level2 .help').fadeIn('fast', function() {
                    helpShake2($('.page-level2 .help'));
                });
            }});
        };

        // eyes
        function eyes2(el) {
            var tl = new TimelineMax({ repeat: 1});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/common/close.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/common/open.png'));
            }});
        };

        function helpShake2(el) {
            var tl = new TimelineMax({ delay: 0.2, repeat: -1, repeatDelay: 1});
            tl.to(el, 0.35, {'right': '+=2%', ease: Power0.easeOut, repeat: 1, yoyo: true, onStart: function() {
                if(canHelp == 0) {
                    tl.kill();
                };
            }});
            tl.to(el, 0.35, {'right': '+=2%', ease: Power0.easeOut, repeat: 1, yoyo: true});
        };

        // help
        var canHelp = 1; 
        $('.page-level2 .help').off('click');
        $('.page-level2 .help').on('click', function() {
            if(canHelp == 1) {
                canHelp = 0;
                $('.page-level2 .help').fadeOut('fast');
                mouseIn2();
            };
        });

        // mouse in
        function mouseIn2() {
            var tl = new TimelineMax({ delay: 0.4 });
            tl.to($('.page-level2 .box-mouse1'), 0.6, { 'top': '1.74rem', ease: Power4.easeOut, onComplete: function() {
                tl.to($('.page-level2 .box-cave'), 0.6, {scale: 0, ease: Power4.easeOut, onComplete: function() {
                    mouseCome2();
                }});
            }});
        };

        // mouse come
        function mouseCome2() {
            var tl = new TimelineMax({ delay: 0.4 });
            tl.to($('.page-level2 .box-dig'), 0.6, { 'right': '15.6%', ease: Power4.easeOut, onComplete: function() {
                dig($('.page-level2 .dig'));
                sugar2();
                sugarDown();
            }});
        };

        // boxing
        function dig(el) {
            var tl = new TimelineMax({ repeat: 51, delay: 0.2 });
            tl.to(el, 0.04, { onComplete: function() {
                el.attr('src', require('../img/level2/dig2.png'));
            }});
            tl.to(el, 0.04, { onComplete: function() {
                el.attr('src', require('../img/level2/dig1.png'));
            }});
        };

        // sugar
        function sugar2() {
            var tl = new TimelineMax({ delay: 0.2 });
            tl.to($('.page-level2 .box-sugar2'), 0.2, {'display': 'block'}, 0);
            tl.to($('.page-level2 .sugar2'), 0.4, {'top': '-2.8rem', 'left': '-400%', ease: Power4.easeOut, onComplete: function() {
                $('.page-level2 .box-sugar2').hide();
            }}, 0.02);
            tl.to($('.page-level2 .line2'), 0.08, {autoAlpha: 1, repeat: 1, yoyo: true}, 0.02);

            tl.to($('.page-level2 .box-sugar3'), 0.2, {'display': 'block'}, 1);
            tl.to($('.page-level2 .sugar3'), 0.4, {'top': '-2.4rem', 'right': '-300%', ease: Power4.easeOut, onComplete: function() {
                $('.page-level2 .box-sugar3').hide();
            }}, 1.02);
            tl.to($('.page-level2 .line3'), 0.08, {autoAlpha: 1, repeat: 1, yoyo: true}, 1.02);

            tl.to($('.page-level2 .box-sugar1'), 0.2, {'display': 'block'}, 2);
            tl.to($('.page-level2 .sugar1'), 0.4, {'top': '-0.96rem', 'left': '-180%', ease: Power4.easeOut, onComplete: function() {
                $('.page-level2 .box-sugar1').hide();
            }}, 2.02);
            tl.to($('.page-level2 .line1'), 0.08, {autoAlpha: 1, repeat: 1, yoyo: true}, 2.02);
        };

        // sugar down
        function sugarDown() {
            var tl = new TimelineMax({ delay: 0.2 });
            tl.to($('.page-level2 .sugars'), 6, {'margin-top': '7rem'}, 0);
            tl.to($('.page-level2 .box-dig'), 4.4, {'top': '12.42rem', onComplete: function() {
                $('.box-arrow1').fadeOut('fast', function() {
                    mouseLeave2();
                });
            }}, 0);
        };

        // mouse leave
        function mouseLeave2() {
            var tl = new TimelineMax({ delay: 0.4 });
            tl.to($('.page-level2 .box-dig'), 0.6, { 'right': '-24.6%', ease: Power4.easeOut, onComplete: function() {
                $('.page-level2 .box-dig').hide();
                $('.page-level2 .text1').fadeOut(400);
                $('.page-level2 .box-fulu1').fadeOut(400);
            }});
            tl.to($('.page-level2 .box-dig'), 0.2, {onComplete: function() {
                $('.page-level2 .re').fadeIn(400, function() {
                    mouseUp2();
                    fuluReShake2($('.page-level2 .re .box-fulumouse'));
                    $('.page-level2 .re .box-arrow2').fadeIn(function() {
                        arrow2Up($('.page-level2 .re .box-arrow2'));
                    });
                });
            }}, '+=0.4');
            tl.to($('.page-level2 .box-dig'), 0.2, {onComplete: function() {
                $('.page-level2 .re .box-tip').fadeIn('fast', function() {
                    tipTwinkle2();
                });
            }}, '+=1.6');
        };

        // fulu shake
        function fuluReShake2(el) {
            var tl = new TimelineMax({ repeat: -1, delay: 0.2 });
            tl.to(el, 0.25, { rotation: 6, ease: Power1.easeOut, repeat: 1, yoyo: true});
            tl.to(el, 0.25, { rotation: -6, ease: Power1.easeOut, repeat: 1, yoyo: true});
        };

        // mouse up
        function mouseUp2() {
            var tl = new TimelineMax({ delay: 0.2 });
            tl.to($('.page-level2 .re .box-mouse2'), 0.8, { 'bottom': '2.34rem', 'right': '-40%', ease: Power4.easeOut, onComplete: function() {
                lantern($('.page-level2 .re .lantern'));
            }});
        };

        // lantern
        function lantern(el) {
            var tl = new TimelineMax({ repeat: -1 });
            tl.to(el, 0.2, {onComplete: function() {
                el.attr('src', require('../img/level2/lantern2.png'));
            }});
            tl.to(el, 0.2, {onComplete: function() {
                el.attr('src', require('../img/level2/lantern3.png'));
            }});
            tl.to(el, 0.2, {onComplete: function() {
                el.attr('src', require('../img/level2/lantern1.png'));
            }});
        };

        // arrow2 up
        function arrow2Up(el) {
            var tl = new TimelineMax({ delay: 0.2, repeat: -1, repeatDelay: 0.2 });
            tl.to(el, 1.6, { autoAlpha: 0, 'top': '-=1rem', ease: Power4.easeOut});
        };

        // tip
        function tipTwinkle2() {
            var tl = new TimelineMax({ repeat: -1, yoyo: true });
            tl.to($('.page-level2 .re .next'), 0.4, {autoAlpha: 1});
        };
    },
    methods: {
        prev() {
            this.$router.replace('/level1');
        },
        next() {
            this.$router.replace('/level3');
        }
    }
};
